#! /bin/bash

# **************************************************************************** #
#                                  Entrypoint                                  #
# **************************************************************************** #

if [ "$#" -lt 1 ]; then
    echo "ERROR: Illegal number of parameters. Syntax: $0 <k8s-namespace> [ --delete-pv | -d ]"
    exit 1
fi

namespace=$1
delete_pv=false

if [[ "$1" == "default" ]]; then
	echo "ERROR: Deletion of the namespace 'default' is forbidden!"
	exit 1
fi
if [[ "$2" ]]; then
	if [[ "$2" == "--delete-pv" || "$2" == "-d" ]]; then
		delete_pv=true
	else
		echo "ERROR: '$2': 2nd positional parameter must be either '--delete-pv' or '-d' !"
		exit 1
	fi
fi


if [ $delete_pv == true ]; then
	# kubectl get pv --namespace dummy | tail -n +2 | grep $NAMESPACE | grep -v Bound | awk '{print $1}' | xargs -I{} kubectl -n dummy delete pv {}
	echo "Ensuring that the volumes provisioned are not retained"
	kubectl get pvc --no-headers -n $namespace | awk '{print $3}' | xargs -I{} kubectl patch pv {} -p '{"spec":{"persistentVolumeReclaimPolicy":"Delete"}}'
fi

kubectl delete ns $namespace
rm -rf "./eos-instance-$namespace"
kubectl config set-context $(kubectl config current-context) --namespace="default"
