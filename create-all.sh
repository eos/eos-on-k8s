#! /bin/bash

# Regular Colors
Color_Off='\033[0m'       # Text Reset
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# **************************************************************************** #
#                                  Utilities                                   #
# **************************************************************************** #

function usage () { 
    echo "Usage: $0 [-n <k8s_namespace>] [-q] [-m <eos-mgm_count>] [-b <eos_image_repo>] [-i <eos_image_tag>] [-u <eos_client_image_tag>] [-c <eos-cli_count>] [-f <eos-fst_count>] [-k mit|heimdal]"
    echo
    echo "-n    Specify desired kubernetes Namespace on which the instance will live (default is 'ns\$(date +%s)')"
    echo "      It must be a compliant DNS-1123 label and match =~ ^[a-z0-9]([-a-z0-9]*[a-z0-9])?$"
    echo "      In practice, must consist of lower case alphanumeric characters or '-', and must start and end with an alphanumeric character"
    echo "-q    Create Pod for QuarkDB server and use QuarkDB instead of In-memory Namespace"
    echo "-m    Specify desired number of MGM servers (default is 1)"
    echo "-b    Specify docker image repository to be used for the Pods creation (default is $EOS_IMAGE_REPO)"
    echo "-i    Specify docker image tag to be used for the Pods creation (default is $EOS_IMAGE_TAG)"
    echo "-u    Specify docker image tag to be used for clients Pods creation (default to EOS_IMAGE_TAG)"
    echo "      Typically, you may want to test debian based client releases "
    echo "-c    Specify desired number of client servers (default is 1)"
    echo "-f    Specify desired number of FST servers (default is 7)"
    echo "-k    Specifiy which Kerberos setup script to use (default is heimdal)"
    echo
    echo "-h    Show usage and exit"
    echo
}

function gen_role () {
    local role=$1
    local role_count=$2
    local pvc=false

    [ $with_pvc == true ] && [[ "kdc" != $role ]] && pvc=true
    image_repo=$EOS_IMAGE_REPO
    [ $role == "cli" ] && image_tag=$EOS_CLI_IMAGE_TAG || image_tag=$EOS_IMAGE_TAG
    
    for i in $(seq 1 $role_count); do
        [[ -z $role_count ]] && i=""
        if [[ $pvc == true ]]; then
            cat "$TEMPLATES_DIR/pvc.template.yaml" "$TEMPLATES_DIR/eos-${role}.template.yaml" > "eos-${role}${i}.yaml"
            sed -i "s/%%%NAMESPACE%%%/${NAMESPACE}/g" "eos-${role}${i}.yaml" # @need to sed before kubectl patch or invalid char
            csplit --quiet --prefix=tmp --digit=2 "eos-${role}${i}.yaml" "/### DEPLOYMENT ###/+1" "{*}"
            kubectl patch --local=true -f tmp01 --patch "$(cat $TEMPLATES_DIR/pv-patch.template.yaml)" -o yaml > tmp02
            cat tmp00 tmp02 > "eos-${role}${i}.yaml"
            rm -f tmp00 tmp01 tmp02
        else
            cp "$TEMPLATES_DIR/eos-${role}.template.yaml" "eos-${role}${i}.yaml"
            sed -i "s/%%%NAMESPACE%%%/${NAMESPACE}/g" "eos-${role}${i}.yaml" # @need to sed before kubectl patch or invalid char
        fi
        sed -i "s/%%%ROLE%%%/${role}/g" "eos-${role}${i}.yaml"
        sed -i "s/%%%NUMBER%%%/${i}/g" "eos-${role}${i}.yaml"
        sed -i "s|%%%IMAGE_REPO%%%|${image_repo}|g" "eos-${role}${i}.yaml"
        sed -i "s/%%%IMAGE_TAG%%%/${image_tag}/g" "eos-${role}${i}.yaml"
    done
}

function get_podname () {
    local app=$1
    kubectl get pods --namespace=$NAMESPACE -l app=$app | grep -E '([0-9]+)/\1' | awk '{print $1}' # Get only READY pods
}

function k8s-exec() {

    local namespace=$NAMESPACE
    local app=$1
    [[ $2 ]] && local k8cmd=${@:2}

    kubectl exec --namespace=$namespace $(kubectl get pods --namespace=$namespace -l app=$app | grep -E '([0-9]+)/\1' | awk '{print $1}') -- /bin/bash -c "$k8cmd"

}


# **************************************************************************** #
#                                  Entrypoint                                  #
# **************************************************************************** #

# defaults
NAMESPACE="ns$(date +%s)"
EOS_IMAGE_REPO="gitlab-registry.cern.ch/dss/eos/eos-ci"
EOS_IMAGE_TAG="4.8.57"
EOS_CLI_IMAGE_TAG=""
CLI_COUNT=1
FST_COUNT=8
MGM_COUNT=1
KRB5="mit"
with_qdb=false
with_pvc=false

while getopts 'n:b:i:u:c:f:qm:vk:h' opt; do
    case "${opt}" in
        n) # a DNS-1123 label must consist of lower case alphanumeric characters or '-', and must start and end with an alphanumeric character
            if [[ $OPTARG =~ ^[a-z0-9]([-a-z0-9]*[a-z0-9])?$ ]]; 
                then NAMESPACE=${OPTARG}
                else echo "! Wrong arg -$opt"; exit 1
            fi ;;
        b)
            EOS_IMAGE_REPO=${OPTARG} ;;
        i)
            EOS_IMAGE_TAG=${OPTARG} ;;
        u)
            EOS_CLI_IMAGE_TAG=${OPTARG} ;;
        c) # clients count must be an integer greater than 0
            if [[ $OPTARG =~ ^[0-9]+$ ]] && [[ $OPTARG -gt 0 ]]; 
                then CLI_COUNT=${OPTARG}
                else echo "! Wrong arg -$opt"; exit 1
            fi ;;
        f) # fsts count must be an integer greater than 0
            if [[ $OPTARG =~ ^[0-9]+$ ]] && [[ $OPTARG -gt 0 ]]; 
                then FST_COUNT=${OPTARG}
                else echo "! Wrong arg -$opt"; exit 1
            fi ;;
        q)
            with_qdb=true ;;
        m) # mgms count must be an integer greater than 0
            if [[ $OPTARG =~ ^[0-9]+$ ]] && [[ $OPTARG -gt 0 ]]; then 
                MGM_COUNT=${OPTARG}
                [[ $OPTARG -gt 1 ]] && with_qdb=true
            else 
                echo "! Wrong arg -$opt"; exit 1
            fi ;;
        v) # @todo parametrize the size of the PersistentVolumeClaim
            with_pvc=true ;;
        k)
            if [[ $OPTARG == "mit" ]] || [[ $OPTARG == "heimdal" ]]; then
                KRB5="${OPTARG}"
            else
                echo "! Wrong arg -$opt"; exit 1
            fi ;;
        h) 
            usage 
            exit 0 ;;
        *)
            usage
            exit 1 ;;
    esac
done
[[ -z $EOS_CLI_IMAGE_TAG ]] && EOS_CLI_IMAGE_TAG=$EOS_IMAGE_TAG # Use same client image version, unless stated otherwise
shift $((OPTIND-1))

echo "NAMESPACE=$NAMESPACE"
echo "EOS_IMAGE_TAG=$EOS_IMAGE_TAG"
echo "EOS_CLI_IMAGE_TAG=$EOS_CLI_IMAGE_TAG"
echo "CLI_COUNT=$CLI_COUNT"
echo "FST_COUNT=$FST_COUNT"
echo "MGM_COUNT=$MGM_COUNT"
echo "with_qdb=$with_qdb"
echo "with_pvc=$with_pvc"
echo "KRB5=$KRB5"


# ********************************************************************************************** 
# Generation of the K8s manifests and configuration scripts for a complete namespaced instance #
# **********************************************************************************************

TEMPLATES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/templates" # one-liner that gives the full directory name of the script no matter where it is being called from.
EOS_INSTANCE_DIR="eos-instance-$NAMESPACE"
mkdir $EOS_INSTANCE_DIR
cd $EOS_INSTANCE_DIR


# Generate the namespace
cp "$TEMPLATES_DIR/namespace.template.yaml" "namespace-$NAMESPACE.yaml"
sed -i "s/%%%NAMESPACE%%%/$NAMESPACE/g" "namespace-$NAMESPACE.yaml"

# Generate the configmap files
cp "$TEMPLATES_DIR/configmap.template.yaml" "configmap.yaml"
sed -i "s/%%%NAMESPACE%%%/${NAMESPACE}/g" "configmap.yaml"
if [[ $with_qdb == true ]]; then # when running in QuarkDB mode we need to change the namespace library loaded by the MGM
    sed -i "s|# sed -i 's/libEosNsInMemory|sed -i 's/libEosNsInMemory|g" configmap.yaml
    echo -e "${Yellow} var 'with_qdb' set to true, EOS will run in QuarkDB mode (use libEosNsQuarkdb.so)${Color_Off}"
fi
if [[ $with_pvc == true ]]; then # let data be on persistent volumes 
    sed -i "s|# sed -i 's@DATADIR=|sed -i 's@DATADIR=|g" configmap.yaml
fi
if [[ $MGM_COUNT -gt 1 ]]; then
    sed -i "s|# echo 'export EOS_USE_QDB_MASTER=1'|echo 'export EOS_USE_QDB_MASTER=1'|g" configmap.yaml
    sed -i "s|# echo 'mgmofs.cfgtype quarkdb'|echo 'mgmofs.cfgtype quarkdb'|g" configmap.yaml
    echo -e "${Yellow} var 'MGM_COUNT' greater than 1, env var EOS_USE_QDB_MASTER will be set${Color_Off}"
    for i in $(seq 1 $MGM_COUNT); do
        cp configmap.yaml configmap-mgm${i}.yaml
        sed -i "s/eos-mgm1.eos-mgm1/eos-mgm${i}.eos-mgm${i}/g" configmap-mgm${i}.yaml
        sed -i "s/configure-container/configure-container-mgm${i}/g" configmap-mgm${i}.yaml
    done
fi

# Generate the manifests
gen_role kdc
gen_role mq
gen_role mgm $MGM_COUNT
gen_role fst $FST_COUNT
gen_role cli $CLI_COUNT
[[ $with_qdb == true ]] && gen_role qdb


# ********************************************************************************************** 
# Deploy the instance #
# ********************************************************************************************** 

shopt -s nullglob # The shopt -s nullglob will make the glob expand to nothing if there are no matches.
roles_yaml=(eos-*.yaml)


# Instantiate the namespace
kubectl apply -f "namespace-$NAMESPACE.yaml"

# Instantiate/ the configmap
if [[ $MGM_COUNT -gt 1 ]]; then
    for i in $(seq 1 $MGM_COUNT); do
        sed -i "s/configure-container/configure-container-mgm${i}/g" eos-mgm${i}.yaml
        kubectl apply -f "configmap-mgm${i}.yaml"
    done
fi
kubectl apply -f "configmap.yaml"


# Conditionally split the pod creation in groups, since apparently the external provisioner (manila?) can't deal with too many volume-creation request per second
# [ $with_pvc == true ] && g=6 || g=${#roles_yaml[@]}
g=${#roles_yaml[@]}
if [[ $with_pvc == true ]]; then
    g=3
    echo -e "${Yellow} var 'with_pvc' set to true, split the creation of ${#roles_yaml[@]} pods in groups to not stress the volume provisioner ${Color_Off}" 
fi  
count=1;
for ((i=0; i < ${#roles_yaml[@]}; i+=g)); do

    for p in ${roles_yaml[@]:i:g}; do
        kubectl apply -f $p; 
    done

    podsReady=$(kubectl get pods --namespace=$NAMESPACE -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}' | grep -o "True" |  wc -l)
    podsReadyExpected=$(( $((i+g))<${#roles_yaml[@]} ? $((i+g)) : ${#roles_yaml[@]} ))
    # [ tty ] && tput sc @todo
    while [[ $count -le 600 ]] && [[ "$podsReady" -lt "$podsReadyExpected" ]]; do
        podsReady=$(kubectl get pods --namespace=$NAMESPACE -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}' | grep -o "True" | wc -l)
        if [[ $(($count%10)) == 0 ]]; then
            # [ tty ] && tput rc @todo
            echo -e "\n${Yellow} Current situation of pods: ${Color_Off}"
            kubectl get pods --namespace=$NAMESPACE
            if [[ $with_pvc == true ]]; then
                echo -e "${Yellow} and persistent volumes: ${Color_Off}"
                kubectl get pv --namespace=$NAMESPACE | grep "$NAMESPACE"
            fi
        fi
        echo -ne "\r Waiting $count secs for $podsReadyExpected pods to be Ready... $podsReady/$podsReadyExpected"
        sleep 1
        ((count+=1))
    done

done

if [[ $count -le 600 ]] ; then
    echo -e "${Green} OK, all the Pods are in Ready state! $podsReady/$podsReadyExpected ${Color_Off}"
else
    echo -e "${Red} KO, not all the Pods are in Ready state! $podsReady/$podsReadyExpected ${Color_Off}"
    exit 1
fi


# ********************************************************************************************** 
# Start the EOS services in each Pod
# ********************************************************************************************** 

# Give the time to execute the readinessProbe, and so the eos-specific configuration of the containers
# kubectl wait pods --all --namespace $NAMESPACE --for=condition=Ready # @note experimental 'kubectl wait' command

echo "Starting the EOS services in each Pod"

echo -e "${Yellow} Exec on eos-kdc... ${Color_Off}"
[[ $KRB5 == "mit"  ]] && k8s-exec eos-kdc "./kdc_mit.sh" || k8s-exec eos-kdc "./kdc.sh"
sleep 1; count=1 
until [[ $count -le 10 ]] && k8s-exec eos-kdc "[ -f /root/eos.keytab ]" ; do
    [[ $KRB5 == "mit"  ]] && k8s-exec eos-kdc "./kdc_mit.sh" || k8s-exec eos-kdc "./kdc.sh"
done;
if [[ $count -gt 10 ]] ; then
    echo -e "${Red} Failed to Exec on kdc ${Color_Off}"
    exit 1
fi
# cp specific keytab from kdc to mgms and clients

TMP=$(mktemp)
kubectl cp $NAMESPACE/$(get_podname eos-kdc):/root/eos.keytab $TMP
for i in $(seq 1 $MGM_COUNT); do
    kubectl cp $TMP $NAMESPACE/$(get_podname eos-mgm${i}):/etc/eos.krb5.keytab
done
rm -f $TMP

TMP=$(mktemp)
kubectl cp $NAMESPACE/$(get_podname eos-kdc):/root/admin1.keytab $TMP
for i in $(seq 1 $CLI_COUNT); do
    kubectl cp $TMP ${NAMESPACE}/$(get_podname eos-cli${i}):/root/admin1.keytab
done
kubectl cp $NAMESPACE/$(get_podname eos-kdc):/root/eos-user.keytab $TMP
for i in $(seq 1 $CLI_COUNT); do
    kubectl cp $TMP ${NAMESPACE}/$(get_podname eos-cli${i}):/home/eos-user/eos-user.keytab
    k8s-exec eos-cli${i} "chown eos-user:eos-user /home/eos-user/eos-user.keytab"
    k8s-exec eos-cli${i} "chmod 600 /home/eos-user/eos-user.keytab"
done
rm -f keytab.tmp

echo -e "${Yellow} Exec on eos-mq... ${Color_Off}"
k8s-exec eos-mq "./eos_mq_setup.sh"

if [[ $with_qdb == true ]]; then 
    echo -e "${Yellow} Exec on eos-qdb... ${Color_Off}"
    k8s-exec eos-qdb "./eos_qdb_setup.sh"
fi

echo -e "${Yellow} Exec on eos-mgms... ${Color_Off}"
for i in $(seq 1 $MGM_COUNT); do
    k8s-exec eos-mgm${i} "./mkcert-ssl.sh"
    k8s-exec eos-mgm${i} "./eos_mgm_setup.sh"
done

echo -e "${Yellow} Exec on every eos-fst... ${Color_Off}"
failure=0; pids="";
for i in $(seq 1 $FST_COUNT); do
    (
        k8s-exec eos-fst${i} "./mkcert-ssl.sh"
        k8s-exec eos-fst${i} "./eos_fst_setup.sh ${i}"
    )&
    pids="${pids} $!"
    sleep 0.1
done
for pid in ${pids}; do
  wait ${pid} || let "failure=1"
done
if [[ "${failure}" == "1" ]]; then
    echo -e "${Red} Failed to Exec on one of the FSTs ${Color_Off}"
    exit 1
fi

echo -e "${Yellow} Exec on eos-mgms (fs setup)... ${Color_Off}"
k8s-exec eos-mgm1 "./eos_mgm_fs_setup.sh ${FST_COUNT}"
if [[ "$?" -ne 0 ]]; then exit 1; fi
# @todo
# for i in $(seq 1 $MGM_COUNT); do
#     k8s-exec eos-mgm${i} "./eos_mgm_fs_setup.sh ${FST_COUNT}"
#     if [[ "$?" -ne 0 ]]; then exit 1; fi
# done

echo -e "${Yellow} Exec on every eos-cli... ${Color_Off}"
for i in $(seq 1 $CLI_COUNT); do
    k8s-exec eos-cli${i} "kinit -kt /root/admin1.keytab admin1@TEST.EOS"
    k8s-exec eos-cli${i} "kvno host/eos-mgm1.eos-mgm1.$NAMESPACE.svc.cluster.local"
    k8s-exec eos-cli${i} "su eos-user -c 'kinit -kt /home/eos-user/eos-user.keytab eos-user@TEST.EOS' "
    k8s-exec eos-cli${i} "su eos-user -c 'kvno host/eos-mgm1.eos-mgm1.$NAMESPACE.svc.cluster.local' "
done

# @todo add error handling
echo -e "${Green} Exec went OK for all the Pods ${Color_Off}"


# Check status
k8s-exec eos-mgm1 "eos fs ls"
for i in $(seq 1 $CLI_COUNT); do
    k8s-exec eos-cli${i} "eos whoami"
    k8s-exec eos-cli${i} "su eos-user -c 'eos whoami' "
done


# print configuration summary
echo ""
echo "NAMESPACE=$NAMESPACE"
echo "EOS_IMAGE_TAG=$EOS_IMAGE_TAG"
echo "EOS_CLI_IMAGE_TAG=$EOS_CLI_IMAGE_TAG"
echo "CLI_COUNT=$CLI_COUNT"
echo "FST_COUNT=$FST_COUNT"
echo "MGM_COUNT=$MGM_COUNT"
echo "with_qdb=$with_qdb"
echo "with_pvc=$with_pvc"
echo "KRB5=$KRB5"
