#! /bin/bash

# Regular Colors
Color_Off='\033[0m'       # Text Reset
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# **************************************************************************** #
#                                  Utilities                                   #
# **************************************************************************** #

function usage () {	
    echo "Usage: $0 [-q] [-b <eos_image_repo>] [-i <eos_image_tag>] -n <k8s_namespace>"
    exit 1
}

function gen_role () {
    local role=$1
    local role_count=$2

    image_repo=$EOS_IMAGE_REPO
    [ $role == "cli" ] && image_tag=$EOS_CLI_IMAGE_TAG || image_tag=$EOS_IMAGE_TAG
    
    for i in $(seq 1 $role_count); do
        [[ -z $role_count ]] && i=""
        cp "$TEMPLATES_DIR/eos-${role}.template.yaml" "eos-${role}${i}.yaml"
        sed -i "s/%%%NAMESPACE%%%/${NAMESPACE}/g" "eos-${role}${i}.yaml"
        sed -i "s/%%%ROLE%%%/${role}/g" "eos-${role}${i}.yaml"
        sed -i "s/%%%NUMBER%%%/${i}/g" "eos-${role}${i}.yaml"
        sed -i "s|%%%IMAGE_REPO%%%|${IMAGE_REPO}|g" "eos-${role}${i}.yaml"
        sed -i "s/%%%IMAGE_TAG%%%/${image_tag}/g" "eos-${role}${i}.yaml"
    done
}


# **************************************************************************** #
#                                  Entrypoint                                  #
# **************************************************************************** #


NAMESPACE="ns$(date +%s)"
EOS_IMAGE_REPO="gitlab-registry.cern.ch/dss/eos/eos-ci"
EOS_IMAGE_TAG="4.8.57"
MGM_COUNT=1
with_qdb=false


while getopts 'n:b:i:q' opt; do
	case "${opt}" in
        n) # a DNS-1123 label must consist of lower case alphanumeric characters or '-', and must start and end with an alphanumeric character
			if [[ $OPTARG =~ ^[a-z0-9]([-a-z0-9]*[a-z0-9])?$ ]]; 
				then NAMESPACE=${OPTARG}
				else echo "! Wrong arg -$opt"; usage
			fi ;;
        b)
            EOS_IMAGE_REPO=${OPTARG} ;;
        i)
            EOS_IMAGE_TAG=${OPTARG} ;;
        q)
            with_qdb=true ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

echo "NAMESPACE=$NAMESPACE"
echo "EOS_IMAGE_TAG=$EOS_IMAGE_TAG"
echo "MGM_COUNT=$MGM_COUNT"
echo "with_qdb=$with_qdb"



# ********************************************************************************************** 
# Generation of the K8s manifests and configuration scripts for a complete namespaced instance #

TEMPLATES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/templates" # one-liner that gives the full directory name of the script no matter where it is being called from.
EOS_INSTANCE_DIR="eos-instance-$NAMESPACE"
mkdir $EOS_INSTANCE_DIR
cd $EOS_INSTANCE_DIR


# Generate the namespace
cp "$TEMPLATES_DIR/namespace.template.yaml" "namespace-$NAMESPACE.yaml"
sed -i "s/%%%NAMESPACE%%%/$NAMESPACE/g" "namespace-$NAMESPACE.yaml"

# Generate the configmap files
cp "$TEMPLATES_DIR/configmap.template.yaml" "configmap.yaml"
sed -i "s/%%%NAMESPACE%%%/${NAMESPACE}/g" "configmap.yaml"
if [[ $with_qdb == true ]]; then # when running in QuarkDB mode we need to change the namespace library loaded by the MGM
    sed -i "s|# sed -i 's/libEosNsInMemory|sed -i 's/libEosNsInMemory|g" configmap.yaml
    echo -e "${Yellow} var 'with_qdb' set to true, EOS will run in QuarkDB mode (use libEosNsQuarkdb.so)${Color_Off}"
fi

# Generate the manifests
gen_role mgm $MGM_COUNT
[[ $with_qdb == true ]] && gen_role qdb


# ********************************************************************************************** 
# Deploy the instance #

shopt -s nullglob # The shopt -s nullglob will make the glob expand to nothing if there are no matches.
roles_yaml=(eos-*.yaml)


# Instantiate the namespace
kubectl apply -f "namespace-$NAMESPACE.yaml"

# Instantiate the configmap
kubectl apply -f "configmap.yaml"



for p in ${roles_yaml[@]}; do 
    kubectl apply -f $p; 
done

podsReady=$(kubectl get pods --namespace=$NAMESPACE -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}' | grep -o "True" |  wc -l)
podsReadyExpected=${#roles_yaml[@]}
[ tty ] && tput sc
count=1;
while [[ $count -le 180 ]] && [[ "$podsReady" -ne "$podsReadyExpected" ]]; do
    podsReady=$(kubectl get pods --namespace=$NAMESPACE -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}' | grep -o "True" | wc -l)
    if [[ $(($count%10)) == 0 ]]; then
        [ tty ] && tput rc
        echo -e "\n${Yellow} Current situation of pods: ${Color_Off}"
        kubectl get pods --namespace=$NAMESPACE
    fi
    echo -ne "\r Waiting $count secs for $podsReadyExpected pods to be Ready... $podsReady/$podsReadyExpected"
    sleep 1
    ((count+=1))
done

if [[ $count -le 180 ]] ; then
    echo -e "${Green} OK, all the Pods are in Ready state! $podsReady/$podsReadyExpected ${Color_Off}"
else
    echo -e "${Red} KO, not all the Pods are in Ready state! $podsReady/$podsReadyExpected ${Color_Off}"
    exit 1
fi


# ********************************************************************************************** 
# Start the EOS services in each Pod

# Give the time to execute the readinessProbe, and so the eos-specific configuration of the containers
# kubectl wait pods --all --namespace $NAMESPACE --for=condition=Ready # @note experimental 'kubectl wait' command

echo "Starting the EOS services in each Pod"

if [[ $with_qdb == true ]]; then 
	echo -e "${Yellow} Exec on eos-qdb... ${Color_Off}"
	kubectl exec $(kubectl get pods --namespace=$NAMESPACE -l app=eos-qdb | grep -E '([0-9]+)/\1' | awk '{print $1}') --namespace=$NAMESPACE -- /bin/bash -c "./eos_qdb_setup.sh"

    
fi

# @todo add error handling
echo -e "${Green} Exec went OK for all the Pods ${Color_Off}"



# tmp
echo ""
echo "NAMESPACE=$NAMESPACE"
echo "EOS_IMAGE_TAG=$EOS_IMAGE_TAG"
echo "MGM_COUNT=$MGM_COUNT"
echo "with_qdb=$with_qdb"
