#! /bin/bash

# Regular Colors
Color_Off='\033[0m'       # Text Reset
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
White='\033[0;37m'        # White

NAMESPACE=ns1582203620
KRB5=mit
function get_podname () {
    local app=$1
    kubectl get pods --namespace=$NAMESPACE -l app=$app | grep -E '([0-9]+)/\1' | awk '{print $1}' # Get only READY pods
}

function k8s-exec() {
    local namespace=$NAMESPACE
    local app=$1
    [[ $2 ]] && local k8cmd=${@:2}
    kubectl exec --namespace=$namespace $(kubectl get pods --namespace=$namespace -l app=$app | grep -E '([0-9]+)/\1' | awk '{print $1}') -- /bin/bash -c "$k8cmd"
}

echo -e "${Yellow} Exec on eos-kdc... ${Color_Off}"
count=1;
while [[ $count -le 10 ]] && (k8s-exec eos-kdc "[ ! -f /root/eos.keytaba ]" &> /dev/null); do
	echo "in"
    [[ $KRB5 == "mit"  ]] && k8s-exec eos-kdc "./kdc_mit.sh" || k8s-exec eos-kdc "./kdc.sh"
    ((count+=1))
done;
if [[ $count -ge 10 ]] ; then
    echo -e "${Red} Failed to Exec on kdc ${Color_Off}"
    exit 1
fi

