stages:
 - test

variables:
  DEFAULT_EOS_TAG: "4.8.94"
  
#-------------------------------------------------------------------------------
# Dock8rnetes testing framework (exec_cmd wraps both docker and k8s!)
#-------------------------------------------------------------------------------

.dock8s_before_script_template: &dock8s_before_script_template
  before_script:
    - case $CI_JOB_NAME in
      "k8s"*  )
        source ./gitlab-ci/before_script_k8s_test.sh;
        source ./gitlab-ci/utilities_func_for_tests.sh --type k8s $K8S_NAMESPACE ;;
      "dock"* )
        source ./gitlab-ci/before_script_docker_test.sh;
        source ./gitlab-ci/utilities_func_for_tests.sh --type docker; ;;
      esac

.dock8s_after_script_template: &dock8s_after_script_template
  after_script:
    - case $CI_JOB_NAME in
      "k8s"*  )
        source ./gitlab-ci/after_script_k8s_test.sh ;;
      "dock"* )
        source ./gitlab-ci/after_script_docker_test.sh ;;
      esac

.dock8s_system_test_template:
  stage: test
  <<: *dock8s_before_script_template
  script:
    - date
    - exec_cmd eos-mgm1 'eos ns mutex --toggleorder'
    - exec_cmd eos-mgm1 'eos-instance-test-ci'
    - date
    - exec_cmd eos-mgm1 'eos-unit-tests-with-instance -n root://localhost//eos/dockertest/'
    - exec_cmd eos-mgm1 'grep "RWMutex. Order Checking Error in thread" /var/log/eos/mgm/xrdlog.mgm && exit 1 || exit 0'
    - date
  <<: *dock8s_after_script_template
  artifacts:
    when: on_failure
    expire_in: 3 days
    paths:
      - eos-logs-${CI_JOB_ID}/
  tags:
    - k8s

.dock8s_cnvrt_fsck_template:
  stage: test
  <<: *dock8s_before_script_template
  script:
    - cp_to_local_cmd eos-cli1:/usr/sbin/eos-test-utils ./eos-test-utils
    # converter
    - cp_to_local_cmd eos-cli1:/usr/sbin/eos-converter-test ./eos-converter-test; chmod +x eos-converter-test
    - case $CI_JOB_NAME in
      "k8s"*  )
        ./eos-converter-test --type k8s $K8S_NAMESPACE ;;
      "dock"* )
        ./eos-converter-test --type docker ;;
      esac
    - rm -rf eos-converter-test
    # fsck
    - cp_to_local_cmd eos-cli1:/usr/sbin/eos-fsck-test ./eos-fsck-test; chmod +x eos-fsck-test
    - case $CI_JOB_NAME in
      "k8s"*  )
        ./eos-fsck-test --max-delay 600 --type k8s $K8S_NAMESPACE ;;
      "dock"* )
        ./eos-fsck-test --max-delay 600 --type docker ;;
      esac
    - rm -rf eos-converter-test
    - rm -rf eos-fsck-test
    - rm -rf eos-test-utils
  <<: *dock8s_after_script_template
  artifacts:
    when: on_failure
    expire_in: 3 days
    paths:
      - eos-logs-${CI_JOB_ID}/

.dock8s_rtb_clone_template:
  stage: test
  <<: *dock8s_before_script_template
  script:
    # prepare mountpoints
    - exec_cmd eos-cli1 'atd; at now <<< "mkdir -p /eos1/ && mount -t fuse eosxd -ofsname=mount-1 /eos1/; mkdir -p /eos2/ && mount -t fuse eosxd -ofsname=mount-2 /eos2/;"'
    - exec_cmd eos-cli1 'count=0; while [[ $count -le 10 ]] && ( [[ ! -d /eos1/dockertest/ ]] || [[ ! -d /eos2/dockertest/ ]] ); do echo "Wait for mount... $count"; (( count++ )); sleep 1; done;'
    # download tests repo
    - exec_cmd eos-cli1 'git clone https://gitlab.cern.ch/dss/eosclient-tests.git'
    # ubuntu releases do not support 'clone' yet, skip its test
    - case $CI_JOB_NAME in
      "ub_bionic"* | "ub_focal"* ) ;;
      *                          ) exec_cmd eos-cli1 'cd /eosclient-tests; clone_tests/clone_test.sh prepare; rc=$?; exit $rc' ;;
      esac
  <<: *dock8s_after_script_template
  artifacts:
    when: on_failure
    expire_in: 3 days
    paths:
      - eos-logs-${CI_JOB_ID}/

.dock8s_fusex_test_template:
  stage: test
  <<: *dock8s_before_script_template
  script:
    # prepare mountpoints
    - exec_cmd eos-cli1 'atd; at now <<< "mkdir -p /eos1/ && mount -t fuse eosxd -ofsname=mount-1 /eos1/; mkdir -p /eos2/ && mount -t fuse eosxd -ofsname=mount-2 /eos2/;"'
    - exec_cmd eos-cli1 'count=0; while [[ $count -le 10 ]] && ( [[ ! -d /eos1/dockertest/ ]] || [[ ! -d /eos2/dockertest/ ]] ); do echo "Wait for mount... $count"; (( count++ )); sleep 1; done;'
    # fusex benchmark
    - exec_cmd eos-mgm1 'eos ns mutex --toggleorder'
    - exec_cmd eos-cli1 'su eos-user -c "mkdir -p /eos1/dockertest/fusex_tests/ && cd /eos1/dockertest/fusex_tests/ && fusex-benchmark"'
    - exec_cmd eos-mgm1 'grep "RWMutex. Order Checking Error in thread" /var/log/eos/mgm/xrdlog.mgm && exit 1 || exit 0'
    # download tests repo
    - exec_cmd eos-cli1 'git clone https://gitlab.cern.ch/dss/eosclient-tests.git'
    # @todo(esindril): run "all" tests in schedule mode once these are properly supported
    # if [[ "$CI_PIPELINE_SOURCE" == "schedule" ]];
    # then
    #   exec_cmd eos-mgm1 'eos vid add gateway "eos-cli1.eos-cli1.${K8S_NAMESPACE}.svc.cluster.local" unix';
    #   exec_cmd eos-cli1 'env EOS_FUSE_NO_ROOT_SQUASH=1 python /eosclient-tests/run.py --workdir="/eos1/dockertest /eos2/dockertest" ci';
    # fi
    # until then just run the "ci" tests
    - exec_cmd eos-cli1 'cd eosclient-tests; for n in prepare/*.sh; do /bin/bash $n prepare; done'
    - exec_cmd eos-cli1 'su eos-user -c "python2 /eosclient-tests/run.py --workdir=\"/eos1/dockertest /eos2/dockertest\" ci"'
    - exec_cmd eos-cli1 'cd eosclient-tests; for n in prepare/*.sh; do /bin/bash $n cleanup; done'
  <<: *dock8s_after_script_template
  artifacts:
    when: on_failure
    expire_in: 3 days
    paths:
      - eos-logs-${CI_JOB_ID}/

.dock8s_cbox_test_template:
  stage: test
  <<: *dock8s_before_script_template
  script:
    # enable converter and prepare eoshome folder, cernbox alike
    - exec_cmd eos-mgm1 'eos space config default space.converter=on'
    - exec_cmd eos-mgm1 './eos_create_userhome.sh eos-user'
    # prepare mountpoints
    - exec_cmd eos-cli1 'atd; at now <<< "mkdir -p /eos/ && mount -t fuse eosxd -ofsname=eosdockertest /eos/"'
    - exec_cmd eos-cli1 'count=0; while [[ $count -le 10 ]] && ( [[ ! -d /eos/ ]] ); do echo "Wait for mount... $count"; (( count++ )); sleep 1; done;'
    # set krb5 ticket and download tests repo @note the 'export KRB5CCNAME to FILE: type' is a spooky trick, can be made nicer.
    - exec_cmd eos-cli1 'echo -e "export KRB5CCNAME=FILE:/tmp/krb5cc_$(id -u eos-user)" >> ~/.bashrc'
    - exec_cmd eos-cli1 'su eos-user -c "kinit eos-user@TEST.EOS -k -t /home/eos-user/eos-user.keytab"'
    - exec_cmd eos-cli1 'su eos-user -c "git clone https://gitlab.cern.ch/dss/eosclient-tests.git /eos/user/e/eos-user/eosclient-tests"'
    # launch the tests
    - exec_cmd eos-cli1 'su eos-user -c "cd /eos/user/e/eos-user && python2 ./eosclient-tests/run.py --workdir=/eos/user/e/eos-user ci-eosfuse_release"'
    - exec_cmd eos-cli1 'su eos-user -c "cd /eos/user/e/eos-user && python2 ./eosclient-tests/run.py --workdir=/eos/user/e/eos-user regression"'
  <<: *dock8s_after_script_template
  allow_failure: true # @todo remove soon, once stable
  artifacts:
    when: on_failure
    expire_in: 3 days
    paths:
      - eos-logs-${CI_JOB_ID}/
  retry: 1

k8s_system:
  extends: .dock8s_system_test_template
  image: alpine/k8s:1.18.2
  tags:
    - k8s

k8s_cnvrt_fsck:
  extends: .dock8s_cnvrt_fsck_template
  image: alpine/k8s:1.18.2
  tags:
    - k8s

k8s_rtb_clone:
  extends: .dock8s_rtb_clone_template
  image: alpine/k8s:1.18.2
  tags:
    - k8s

k8s_fusex:
  extends: .dock8s_fusex_test_template
  image: alpine/k8s:1.18.2
  tags:
    - docker_node
    - k8s

k8s_cbox:
  extends: .dock8s_cbox_test_template
  image: alpine/k8s:1.18.2
  tags:
    - k8s

k8s_stress:
  stage: test
  image: alpine/k8s:1.18.2
  <<: *dock8s_before_script_template
  script:
    - TEST_URL="eos-mgm1.eos-mgm1.$K8S_NAMESPACE.svc.cluster.local"
    - exec_cmd eos-mgm1 "yum install -y grid-hammer"
    - exec_cmd eos-mgm1 "hammer-runner.py --strict-exit-code 1 --gitlab --url ${TEST_URL}//eos/dockertest/hammer/ --protocols xroot --threads 1 2 10 100 --operations write stat read delete --runs 3 --nfiles 10000"
  <<: *dock8s_after_script_template
  # @todo(esindril) remove once we have grid-hammer built with XRootD5
  allow_failure: true
  tags:
    - k8s

k8s_system_c8:
  extends: .dock8s_system_test_template
  image: alpine/k8s:1.18.2
  variables:
    BASETAG: "c8_"
  tags:
    - k8s

k8s_cbox_c8:
  extends: .dock8s_cbox_test_template
  image: alpine/k8s:1.18.2
  variables:
    BASETAG: "c8_"
  tags:
    - k8s

k8s_fusex_ub_focal:
  extends: .dock8s_fusex_test_template
  image: alpine/k8s:1.18.2
  variables:
    CLI_BASETAG: "ubuntu_focal_client_"
  tags:
    - k8s

.unit_test_template: &unit_test_template_definition
  stage: test
  script:
    # generic unit tests
    - eos-unit-tests
    - eos-unit-tests-fst
    - eos-fusex-tests
    # namespace specific unit tests
    - export EOS_QUARKDB_HOSTPORT=localhost:7777
    - quarkdb-create --path /var/quarkdb/node-0
    - chown -R daemon:daemon /var/quarkdb/node-0
    - /usr/bin/xrootd -n qdb -c /etc/xrd.cf.quarkdb -l /var/log/eos/xrdlog.qdb -b -Rdaemon
    - eos-ns-quarkdb-tests
  tags:
     - dock

unit_test:tag:
  image:
    name: gitlab-registry.cern.ch/dss/eos/eos-ci:${DEFAULT_EOS_TAG}
    entrypoint: ["/bin/bash", "-c"]
  <<: *unit_test_template_definition
